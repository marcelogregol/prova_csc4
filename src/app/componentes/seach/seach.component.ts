import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-seach',
  templateUrl: './seach.component.html',
  styleUrls: ['./seach.component.css']
})
export class SeachComponent implements OnInit {

  result: any;
  resultweather: any;
  resultweatherDay: any;
  resultweatherFloripa: any;
  resultweatherManaus: any;
  resultweatherRio: any;
  loading: Boolean = false;
  showpainel: Boolean = true;
  queryField = new FormControl();


  constructor(private service: WeatherService) { }

  ngOnInit(): void {
    this.buscaFloripa();
    this.buscaManaus();
    this.buscaRio();

  }

  onSearch() {
    this.loading = true;
    console.log(this.queryField.value);
    this.service.getCity(this.queryField.value).subscribe(
      data => {
        this.buscarClima(data[0]['Key']);
        this.buscaDay(data[0]['Key']);
        this.result = data;
        this.showpainel = true;
        this.loading = false;
      }
    )


  }


  buscarClima(keyCity: string) {
    this.service.getWeatherbyCity(keyCity).subscribe(
      data => {
        this.resultweather = data['DailyForecasts'];
      }
    )

  }

  buscaDay(keyDay: string) {
    this.service.getWatherbyCityDay(keyDay).subscribe(
      data => {
        console.log(data['DailyForecasts'])
        // alert(data['DailyForecasts'][0]['Date'])

        this.resultweatherDay = data['DailyForecasts'];
      }
    )
  }

  buscaFloripa() {
    this.service.getFloripa().subscribe(
      data =>{
      console.log(data)
        this.resultweatherFloripa = data['DailyForecasts'];
      },
      error => {
        console.log(error);
      }
    )
  }

  buscaManaus() {
    this.service.getManaus().subscribe(
      data =>{
      console.log(data)
        this.resultweatherManaus = data['DailyForecasts'];
      },
      error => {
        console.log(error);
      }
    )
  }

  buscaRio() {
    this.service.getRio().subscribe(
      data =>{
      console.log(data)
        this.resultweatherRio = data['DailyForecasts'];
      },
      error => {
        console.log(error);
      }
    )
  }


  closePanel(){
    this.showpainel = false;
  }

}
