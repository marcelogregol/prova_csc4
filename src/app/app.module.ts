import { CrudService } from './services/crud.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrudComponent } from './componentes/crud/crud.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { SeachComponent } from './componentes/seach/seach.component';
import { ReactiveFormsModule } from '@angular/forms';
import { WeatherService } from './services/weather.service';

@NgModule({
  declarations: [
    AppComponent,
    CrudComponent,
    SeachComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [CrudService, WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
