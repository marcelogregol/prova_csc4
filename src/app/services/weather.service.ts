import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHandler } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {


  url = 'http://dataservice.accuweather.com/locations/v1/cities/search?apikey=owZ56bfrgWqDOIpEQt4SOiYQ06hl0R4d&q=';

  urlWeather = '';

  constructor(private http: HttpClient) {

  }

  getCity(cidade: string) {
    return this.http.get(this.url + cidade);

  }

  getWeatherbyCity(cidadekey: string) {
    return this.http.get(`http://dataservice.accuweather.com/forecasts/v1/daily/5day/${cidadekey}?apikey=owZ56bfrgWqDOIpEQt4SOiYQ06hl0R4d&metric=true&details=true`);
  }

  getWatherbyCityDay(cidadekey: string) {
    return this.http.get(`http://dataservice.accuweather.com/forecasts/v1/daily/1day/${cidadekey}?apikey=owZ56bfrgWqDOIpEQt4SOiYQ06hl0R4d&metric=true&details=true`);
  }

  getFloripa() {
    return this.http.get(`http://dataservice.accuweather.com/forecasts/v1/daily/1day/35952?apikey=owZ56bfrgWqDOIpEQt4SOiYQ06hl0R4d&metric=true&details=true`);
  }

  getManaus() {
    return this.http.get(`http://dataservice.accuweather.com/forecasts/v1/daily/1day/42471?apikey=owZ56bfrgWqDOIpEQt4SOiYQ06hl0R4d&metric=true&details=true`);
  }

  getRio() {
    return this.http.get(`http://dataservice.accuweather.com/forecasts/v1/daily/1day/45449?apikey=owZ56bfrgWqDOIpEQt4SOiYQ06hl0R4d&metric=true&details=true`);
  }


}


